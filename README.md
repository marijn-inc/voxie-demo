# Voxie Contact Import APp

### Setup

.env and .env.testing are added for convenience only

* create a database called voxie_demo with collation utf8mb

* create a database called voxie_demo_testing with collation utf8mb

* open .env and update for your environment

APP_URL=[your domain] (e.g http://voxie.demo.local)
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=voxie_demo
DB_USERNAME=
DB_PASSWORD=

* open .env.testing and update for your environment

APP_URL=[your domain] (e.g http://voxie.demo.local)
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=voxie_demo_testing
DB_USERNAME=
DB_PASSWORD=

* run `composer install`

* run `npm install`

* run `php artisan migrate`

* run `php artisan optimize`

### Testing

* run `phpunit` from the project root (all tests should pass)

