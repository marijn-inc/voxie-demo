<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id',
        'unsubscribed_status',
        'first_name',
        'last_name',
        'phone',
        'email',
        'sticky_phone_number_id',
        'twitter_id',
        'fb_messenger_id',
        'time_zone',
        'contact_list_tag_id'
    ];

    public function setTeamIdAttribute($value)
    {
        $this->attributes['team_id'] = (integer)$value;
    }

    public function setUnsubscribedStatusAttribute($value)
    {
        $this->attributes['unsubscribed_status'] = (string)$value;
    }

    public function setFindNameAttribute($value)
    {
        $this->attributes['first_name'] = (string)$value;
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = (string)$value;
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = (string)$value;
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = (string)$value;
    }

    public function setStickyPhoneNumberIdAttribute($value)
    {
        $this->attributes['sticky_phone_number_id'] = (integer)$value;
    }

    public function setTwitterIdAttribute($value)
    {
        $this->attributes['twitter_id'] = (string)$value;
    }

    public function setfbMessengerIdAttribute($value)
    {
        $this->attributes['fb_messenger_id'] = (string)$value;
    }

    public function setTimeZoneAttribute($value)
    {
        $this->attributes['time_zone'] = (string)$value;
    }

    public function customContactFields()
    {
        return $this->hasMany('App\CustomContactField');
    }
}
