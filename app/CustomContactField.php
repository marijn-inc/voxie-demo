<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomContactField extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'field_name',
        'field_value',
        'contact_id'
    ];

    public function setFieldNameAttribute($value)
    {
        $this->attributes['field_name'] = (string)$value;
    }

    public function setFieldNameValue($value)
    {
        $this->attributes['field_value'] = (string)$value;
    }
}
