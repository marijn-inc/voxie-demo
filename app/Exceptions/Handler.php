<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // return parent::render($request, $exception);
        $errors = env('APP_DEBUG') ?
            [ [ $exception->getTraceAsString() ] ] :
            [ [ __('validation.system.internalError') ] ];

        $class = get_class($exception);
        switch($class) {
            case 'Illuminate\Validation\ValidationException':
                $code = $exception->status;
                $errors = $exception->errors();
                break;
            case 'Symfony\Component\HttpKernel\Exception\NotFoundHttpException':
                $code = Response::HTTP_NOT_FOUND;
                break;
            default:
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
                break;
        }

        if (env('APP_DEBUG')) {
            return response()->json(
                $this->parseVerbose($request, $errors, $code),
                $code
            );
        }

        return response()->json([ "errors" => $this->normalizeErrors($errors) ], $code);
    }

    private function normalizeErrors($errors) {
        $normalized = [];
        foreach($errors as $errorList) {
            foreach($errorList as $error) {
                $normalized[] = $error;
            }
        }
        return $normalized;
    }

    private function parseVerbose($request, $errors, $code)
    {
        return [
            "resource" => $request->url(),
            "contents" => $request->json()->all(),
            "errors" => $this->normalizeErrors($errors),
            "code" => $code
        ];
    }
}
