<?php

namespace App\Http\Controllers;

use App\Http\Resources\ContactCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApiController extends Controller
{
    public function createContactList(Request $request)
    {
        $params = $request->json()->all();

        $contactsCollection =
            app()->make('App\Services\ContactService')->createContactList($params);

        return (new ContactCollection($contactsCollection))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function getContactProps()
    {
        $contactProps = app()->make('App\Services\ContactService')->getContactColumnSpecs();
        return response()->json([ "data" => $contactProps ], Response::HTTP_OK);
    }
}
