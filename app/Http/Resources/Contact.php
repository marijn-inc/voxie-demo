<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Contact extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $arr = [
            'id' => $this->id,
            'team_id' => $this->team_id,
            'unsubscribed_status' => $this->unsubscribed_status,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'sticky_phone_number_id' => $this->sticky_phone_number_id,
            'twitter_id' => $this->twitter_id,
            'fb_messenger_id' => $this->fb_messenger_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'time_zone' => $this->time_zone
        ];

        $customContactFields = $this->customContactFields()->get();
        foreach($customContactFields as $customContactField) {
            $arr[$customContactField->field_name] = $customContactField->field_value;
        }

        return $arr;
    }
}


