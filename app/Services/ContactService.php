<?php


namespace App\Services;

use App\Contact;
use App\ContactListTag;
use App\CustomContactField;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ContactService
{
    public function createContact($params): Contact {

        $validator = Validator::make(
            $params,
            [
                'team_id' => 'numeric',
                'unsubscribed_status' => 'string',
                'first_name' => 'string',
                'last_name' => 'string',
                'phone' => 'string',
                'email' => 'required|email',
                'sticky_phone_number_id' => 'numeric',
                'twitter_id' => 'string',
                'fb_messenger_id' => 'string',
                'time_zone' => 'string'
            ]
        );
        if ($validator->fails()) {
            $e = ValidationException::withMessages($validator->getMessageBag()->all());
            $e->status(Response::HTTP_BAD_REQUEST);
            throw $e;
        }

        $contact = new Contact($params);
        $contact->save();

        return $contact;
    }

    public function createContactList($params): Collection
    {
        $validator = Validator::make(
            $params,
            [
                'name' => 'required|string',
                'file' => 'required|string',
                'mappings' => 'required|array',
                'updateStrategy' => 'required|in:updateEmpty,updateOverwrite,noUpdate'
            ]
        );
        if ($validator->fails()) {
            $e = ValidationException::withMessages($validator->getMessageBag()->all());
            $e->status(Response::HTTP_BAD_REQUEST);
            throw $e;
        }

        $contacts = $this->createContactListFromColumnMapping(
            $params['name'],
            base64_decode($params['file']),
            $params['mappings'],
            $params['updateStrategy']
        );

        return $contacts;
    }

    public function getContactColumnSpecs(): array {
        $columns = ['custom'];
        $columns = array_merge($columns, Schema::getColumnListing('contacts'));

        $columns = array_filter($columns,function($column) {
            if(in_array($column , ['id','created_at','updated_at','contact_list_tag_id'])) {
                return false;
            }
            return true;
        });

        return array_values($columns);
    }

    private function createContactListFromColumnMapping(string $name, string $file, array $mapping, string $strategy): Collection
    {
        $resolvedContactListArray = $this->resovleContactColumnMappings($file, $mapping);
        return $this->createContactListFromArray($name, $strategy, $resolvedContactListArray);
    }

    private function resovleContactColumnMappings(string $file, array $mapping): array
    {
        // Dump the file contents to disk (take advantage of the file function)
        $tempPath = storage_path() . '/framework/cache/data/' . time();
        file_put_contents($tempPath, $file);

        $columnMappings = [];

        $csv = array_map('str_getcsv', file($tempPath));
        foreach ($csv[0] as $index => $columnName) {
            if (array_key_exists($columnName, $mapping) &&
                $mapping[$columnName] !== 'custom') {
                $columnMappings[] = $mapping[$columnName];
            } else {
                $columnMappings[] = $columnName;
            }
        }

        $csv[0] = $columnMappings;
        array_walk($csv, function (&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header

        // Clean up the temp file
        unlink($tempPath);

        return $csv;
    }

    private function createContactListFromArray(string $name, string $strategy, array $contactListArray): Collection
    {
        $startId = null;
        DB::transaction(function() use ($contactListArray, $name, $strategy, &$startId){

            // For each contact in the list, parse the legit columns and create the contacts
            $contactAttributes = [];
            foreach($contactListArray as $contact) {
                $contactInsert = [];
                $customInserts = [];
                foreach($contact as $columnName => $columnValue) {
                    if (in_array($columnName, Schema::getColumnListing('contacts'))) {
                        $contactInsert[$columnName] = $columnValue;
                    } else {
                        $customInserts[] = [
                            'field_name' => $columnName,
                            'field_value' => $columnValue
                        ];
                    }
                }
                if($contactInsert) {
                    $contactAttributes[] = [
                        $contactInsert, // The contact row
                        $customInserts // The custom field rows for this contact
                    ];
                }
            }

            // Add the contacts
            if(!$contactAttributes) {
                return new Collection();
            }

            // TODO: Would prefer Contact/CustomContactField::insert but mutators won't execute on bulk DB::insert
            foreach($contactAttributes as $contactAttribute) {

                $contactListTag = ContactListTag::where('tag','=', $name)->first();
                if(!$contactListTag) {
                    $contactListTag = new ContactListTag(['tag' => $name ]);
                    $contactListTag->save();
                }

                $contactAttribute[0]['contact_list_tag_id'] = $contactListTag->id;

                $contact = Contact::create($contactAttribute[0]);
                if(!$startId) {
                    $startId = $contact->id;
                }

                foreach($customInserts as $customInsert) {
                    $customInsert['contact_id'] = $contact->id;
                    CustomContactField::create($customInsert);
                }
            }

        });

        return Contact::where('id', '>=', $startId)->get();

    }
}
