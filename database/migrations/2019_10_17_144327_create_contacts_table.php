<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {

            // ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            // id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            $table->bigIncrements('id', 20);

            // team_id int(10) unsigned NOT NULL,
            $table->unsignedInteger('team_id')->default(0);

            // unsubscribed_status varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
            $table->string('unsubscribed_status', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(false)
                ->default('none');

            // first_name varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            $table->string('first_name', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            // last_name varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            $table->string('last_name', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            // phone varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
            $table->string('phone', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(false)
                ->default('0');

            // email varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            $table->string('email', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            // sticky_phone_number_id int(11) DEFAULT NULL,
            $table->integer('sticky_phone_number_id')
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            // twitter_id varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            $table->string('twitter_id', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            // fb_messenger_id varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            $table->string('fb_messenger_id', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            // created_at timestamp NULL DEFAULT NULL,
            $table->timestamp('created_at')->nullable(true);

            // updated_at timestamp NULL DEFAULT NULL,
            $table->timestamp('updated_at')->nullable(true);

            // time_zone varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            $table->string('time_zone', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            // Additonal foreign key added to associate contacts to tags
            $table->bigInteger('contact_list_tag_id')->unsigned()->nullable(true);
            $table->foreign('contact_list_tag_id')->references('id')->on('contact_list_tags')->onDelete('cascade');

            // KEY idx_phone_search (`team_id`,`phone`),
            $table->index(['team_id', 'phone'], 'idx_phone_search');

            // KEY contacts_team_id_index (`team_id`),
            $table->index(['team_id'], 'contacts_team_id_index');

            // KEY contacts_first_name_index (`first_name`),
            $table->index(['first_name'], 'contacts_first_name_index');

            // KEY contacts_last_name_index (`last_name`),
            $table->index(['last_name'], 'contacts_last_name_index');

            // KEY contacts_phone_index (`phone`),
            $table->index(['phone'], 'contacts_phone_index');

            // KEY contacts_email_index (`email`),
            $table->index(['email'], 'contacts_email_index');

            // KEY contacts_twitter_id_index (`twitter_id`),
            $table->index(['twitter_id'], 'contacts_twitter_id_index');

            // KEY contacts_fb_messenger_id_index (`fb_messenger_id`)
            $table->index(['fb_messenger_id'], 'contacts_fb_messenger_id_index');

        });

        DB::update("ALTER TABLE contacts AUTO_INCREMENT=256;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}



