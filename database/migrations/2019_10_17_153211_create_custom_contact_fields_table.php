<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomContactFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_contact_fields', function (Blueprint $table) {

            // ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->string('field_name', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            $table->string('field_value', 191)
                ->collation('utf8mb4_unicode_ci')
                ->nullable(true);

            $table->bigInteger('contact_id')->unsigned();
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');

            // created_at timestamp NULL DEFAULT NULL,
            $table->timestamp('created_at')->nullable(true);

            // updated_at timestamp NULL DEFAULT NULL,
            $table->timestamp('updated_at')->nullable(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_contact_fields');
    }
}
