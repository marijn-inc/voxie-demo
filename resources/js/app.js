import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(VueRouter)
Vue.use(VueResource)

import App from './views/App'
import Home from './components/Home'
import NotFound from './components/NotFound'
import Sidebar from './components/Sidebar'
import ImportContacts from './components/ImportContacts'
import ImportContactsProgressButton from "./components/ImportContactsProgressButton"
import ImportContactsFieldMapping from "./components/ImportContactsFieldMapping"
import ImportContactsProgressHeader from "./components/ImportContactsProgressHeader"
import ImportContactsStepInstructions from "./components/ImportContactsStepInstructions"
import ImportContactsFileUpload from "./components/ImportContactsFileUpload"
import ImportContactsStrategy from "./components/ImportContactsStrategy"
import ImportContactsAddName from "./components/ImportContactsAddName"
import LoadingSpinner from "./components/LoadingSpinner";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: App,
            children: [
                {
                    path: 'home',
                    component: Home,
                    children: [
                        {
                            path: 'import-contacts',
                            component: ImportContacts
                        }
                    ]
                },
                {
                    path: '*',
                    beforeEnter: (to, from, next) => next('/home/import-contacts')
                }
            ]
        },
        {
            path: '*',
            component: NotFound
        }
    ],
});

Vue.component('side-bar', Sidebar);
Vue.component('import-contacts-progress-button', ImportContactsProgressButton);
Vue.component('import-contacts-field-mapping', ImportContactsFieldMapping);
Vue.component('import-contacts-progress-header', ImportContactsProgressHeader);
Vue.component('import-contacts-step-instructions', ImportContactsStepInstructions);
Vue.component('import-contacts-file-upload', ImportContactsFileUpload);
Vue.component('import-contacts-strategy', ImportContactsStrategy);
Vue.component('import-contacts-add-name', ImportContactsAddName);
Vue.component('loading-spinner', LoadingSpinner);

const app = new Vue({
    el: '#app',
    components: {
        App,
        Home,
        Sidebar,
        ImportContacts,
        NotFound
    },
    router
});





