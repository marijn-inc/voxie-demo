<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Voxie Contacts App</title>
    <!-- Custom fonts for this template-->
    <link href="{{ mix('css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ mix('css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="app">
        <app></app>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/jquery.min.js') }}"></script>
    <script src="{{ mix('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ mix('js/jquery.easing.min.js') }}"></script>
    <script src="{{ mix('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ mix('js/papaparse.min.js') }}"></script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">




