<?php

namespace Tests\Unit;

use App\Contact;
use App\CustomContactField;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class CreateContactFromColumnMappingTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify the creation of the contact using a column mapping via the service.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactListFromColumnMappingTest()
    {
        $params = [
            'name' => 'MyList',
            'file' => $this->createTestUploadFile(),
            'mappings' => [
                'CompanyName' => 'twitter_id',
                'Street' => 'unsubscribed_status',
                'City' => 'first_name',
                'State' => 'last_name',
                'ZIP' => 'custom',
                'Phone' => 'custom',
                'Website' => 'custom'
            ],
            'updateStrategy' => 'updateEmpty'
        ];

        $contactsCollection = app()->make('App\Services\ContactService')->createContactList($params);

        // Verify the correct number of contacts were created
        $this->assertEquals($contactsCollection->count(), Contact::count());

        // Verify the correct number of custom fields were created
        $this->assertEquals($contactsCollection->count() * 3, CustomContactField::count());
    }

    /**
     * A test to verify the creation of the contact, using a non-expected data types for columns, via the service.  For example, team_id and
     * sticky_phone_number_id are both integers but if we map them to strings, it should not break the import.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactListFromNonNativeColumnMappingTest()
    {
        $params = [
            'name' => 'MyList',
            'file' => $this->createTestUploadFile(),
            'mappings' => [
                'CompanyName' => 'team_id',
                'Street' => 'sticky_phone_number_id',
                'City' => 'first_name',
                'State' => 'last_name',
                'ZIP' => 'custom',
                'Phone' => 'custom',
                'Website' => 'custom'
            ],
            'updateStrategy' => 'updateEmpty'
        ];

        $contactsCollection = app()->make('App\Services\ContactService')->createContactList($params);

        // Verify the correct number of contacts were created
        $this->assertEquals($contactsCollection->count(), Contact::count());

        // Verify the correct number of custom fields were created
        $this->assertEquals($contactsCollection->count() * 3, CustomContactField::count());
    }

    private function createTestUploadFile() {
        return base64_encode(file_get_contents(getcwd() . '/tests/Feature/testContactList.csv'));
    }
}
