<?php

namespace Tests\Unit;

use App\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CreateContactModelInvalidFbMessengerIdTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify that a proper validation error is thrown when the fb messenger id is invalid.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactModelInvalidFbMessengerIdTest()
    {
        try {

            app()->make('App\Services\ContactService')->createContact(
                [
                    'fb_messenger_id' => 348867240
                ]
            );

            $this->assertTrue(false);

        } catch(ValidationException $exception) {

            $this->assertEquals($exception->status, Response::HTTP_BAD_REQUEST);

        }
    }
}
