<?php

namespace Tests\Unit;

use App\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CreateContactModelInvalidFirstNameTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify that a proper validation error is thrown when the first name is invalid.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactModelInvalidFirstNameTest()
    {
        try {

            app()->make('App\Services\ContactService')->createContact(
                [
                    'first_name' => 000000
                ]
            );

            $this->assertTrue(false);

        } catch(ValidationException $exception) {

            $this->assertEquals($exception->status, Response::HTTP_BAD_REQUEST);

        }
    }
}
