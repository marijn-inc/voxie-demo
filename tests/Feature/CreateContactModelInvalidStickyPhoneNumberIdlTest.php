<?php

namespace Tests\Unit;

use App\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CreateContactModelInvalidStickyPhoneNumberTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify that a proper validation error is thrown when the sticky phone number is invalid.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactModelInvalidStickyPhoneNumberTest()
    {
        try {

            app()->make('App\Services\ContactService')->createContact(
                [
                    'sticky_phone_number_id' => 'aaaa'
                ]
            );

            $this->assertTrue(false);

        } catch(ValidationException $exception) {

            $this->assertEquals($exception->status, Response::HTTP_BAD_REQUEST);

        }
    }
}
