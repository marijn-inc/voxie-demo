<?php

namespace Tests\Unit;

use App\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CreateContactModelInvalidUnsubscribedStatusTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify that a proper validation error is thrown when the unsubscribed status is invalid.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactModelInvalidUnsubscribedStatusTest()
    {
        try {

            app()->make('App\Services\ContactService')->createContact(
                [
                    'unsubscribed_status' => 000000
                ]
            );

            $this->assertTrue(false);

        } catch(ValidationException $exception) {

            $this->assertEquals($exception->status, Response::HTTP_BAD_REQUEST);

        }
    }
}
