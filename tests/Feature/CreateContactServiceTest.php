<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateContactWithServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify the creation of the contact via the service.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactWithServiceTest()
    {
        $contact = app()->make('App\Services\ContactService')->createContact(
            [
                'team_id' => 100,
                'unsubscribed_status' => 'subscribed',
                'first_name' => 'Michael',
                'last_name' => 'Trussler',
                'phone' => '(555)-555-0000',
                'email' => 'michael@mail.com',
                'sticky_phone_number_id' => 1000,
                'twitter_id' => '@mtrussler',
                'fb_messenger_id' => '348867240',
                'time_zone' => 'GMT-5'
            ]
        );

        $this->assertEquals($contact->team_id, 100);
        $this->assertEquals($contact->unsubscribed_status, 'subscribed');
        $this->assertEquals($contact->first_name, 'Michael');
        $this->assertEquals($contact->last_name, 'Trussler');
        $this->assertEquals($contact->phone, '(555)-555-0000');
        $this->assertEquals($contact->email, 'michael@mail.com');
        $this->assertEquals($contact->sticky_phone_number_id, 1000);
        $this->assertEquals($contact->twitter_id, '@mtrussler');
        $this->assertEquals($contact->fb_messenger_id, '348867240');
        $this->assertEquals($contact->time_zone, 'GMT-5');
    }
}
