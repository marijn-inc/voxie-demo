<?php

namespace Tests\Unit;

use App\Contact;
use App\CustomContactField;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class CreateContactFromColumnMappingHttpTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify the creation of the contact using a column mapping via http request
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testCreateContactListFromColumnMappingTest()
    {
        $params = [
            'name' => 'MyList',
            'file' => $this->createTestUploadFile(),
            'mappings' => [
                'CompanyName' => 'team_id',
                'Street' => 'sticky_phone_number_id',
                'City' => 'first_name',
                'State' => 'last_name',
                'ZIP' => 'custom',
                'Phone' => 'custom',
                'Website' => 'custom'
            ],
            'updateStrategy' => 'updateEmpty'
        ];

        $response = $this->json('POST', '/api/contact-list', $params);
        $response->assertStatus(201);
    }

    private function createTestUploadFile() {
        return base64_encode(file_get_contents(getcwd() . '/tests/Feature/testContactList.csv'));
    }
}
