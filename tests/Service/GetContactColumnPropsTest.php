<?php

namespace Tests\Service;

use App\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetContactColumnPropsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify the correct fetch of the column properties for contacts via http request
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testGetContactColumnPropsTest()
    {
        $columnProps = [
            'custom',
            'team_id',
            'unsubscribed_status',
            'first_name',
            'last_name',
            'phone',
            'email',
            'sticky_phone_number_id',
            'twitter_id',
            'fb_messenger_id',
            'time_zone'
        ];

        $response = $this->json('GET', '/api/contact-properties');
        $response->assertStatus(200);
    }
}
