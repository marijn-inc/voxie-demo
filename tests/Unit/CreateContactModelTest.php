<?php

namespace Tests\Unit;

use App\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateContactModelTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to verify the creation of the contact model.
     *
     * @return void
     */
    public function testCreateContactModelTest()
    {
        $contact = new Contact();
        $contact->team_id = 100;
        $contact->unsubscribed_status = 'subscribed';
        $contact->first_name = 'Michael';
        $contact->last_name = 'Trussler';
        $contact->phone = '(555)-555-0000';
        $contact->email = 'michael@mail.com';
        $contact->sticky_phone_number_id = 1000;
        $contact->twitter_id = '@mtrussler';
        $contact->fb_messenger_id = '348867240';
        $contact->time_zone = 'GMT-5';
        $contact->save();

        $this->assertDatabaseHas('contacts', [
            'team_id' => 100,
            'unsubscribed_status' => 'subscribed',
            'first_name' => 'Michael',
            'last_name' => 'Trussler',
            'phone' => '(555)-555-0000',
            'email' => 'michael@mail.com',
            'sticky_phone_number_id' => 1000,
            'twitter_id' => '@mtrussler',
            'fb_messenger_id' => '348867240',
            'time_zone' => 'GMT-5'
        ]);
    }
}
