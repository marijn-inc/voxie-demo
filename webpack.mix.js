const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('./resources/js/app.js', 'public/js')
    .copy('./resources/vendor/jquery/jquery.min.js', 'public/js/jquery.min.js')
    .copy('./resources/vendor/jquery-easing/jquery.easing.min.js', 'public/js/jquery.easing.min.js')
    .copy('./resources/vendor/bootstrap/js/bootstrap.bundle.min.js', 'public/js/bootstrap.bundle.min.js')
    .copy('./resources/js/sb-admin-2.min.js', 'public/js/sb-admin-2.min.js')
    .copy('./node_modules/papaparse/papaparse.min.js', 'public/js/papaparse.min.js')
    .copyDirectory('./resources/vendor/fontawesome-free/webfonts', 'public/webfonts')
    .sass('./resources/sass/app.scss', 'public/css')
    .styles(['./resources/css/app.css' ], 'public/css/app.css')
    .styles(['./resources/vendor/fontawesome-free/css/all.min.css' ], 'public/css/all.min.css')
    .styles(['./resources/css/sb-admin-2.min.css'], 'public/css/sb-admin-2.min.css');
